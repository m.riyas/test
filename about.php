<?php 
$home="";
$nipah="";
$Covid19="";
$Knowledge_Management="";
$Information_to_Public="";
$health_data_box="";
$Kerala_Health="active"; 
include('header.php');
?>

  <main id="main">

    <!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>About Us</h2>
          <ol>
            <li><a href="#">Kerala Health</a></li> 
            <li><a href="#">About Us</a></li> 
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section class="inner-page">
      <div class="container">
        <section id="adv" class="adv section-bg">
    <div class="container"> 
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <img src="images/healthcare.jpg" style="width: 100%">
        </div>
        <div class="col-md-6 col-sm-12">
          <p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;Historically, the princely rules of the state, made a small beginning to provide infrastructural facilities for a primary health care system. After the re-organisation of the state, it has reached a fairly high level of standard and soundness. The availability of facilities for primary health care, their accessibility, the very high degree of awareness and acceptability among the people have made in Kerala model an almost perfect one. What is needed at present is to sustain these, by the personnel involved with the active participation and co-operation of the people.With the effective involvement of the private sector which plays a major role in the health sector and with the effects of voluntary organisations this task though throws a challenge is attainable. </p>
        </div>
      </div>
      
      

          <p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;The level of achievements attained in the implementation of the various national programmes for control / eradication of diseases and also of family welfare programme including universal immunisation programme and maternal and child health activities has helped the state to reduce the mortality rates and improve the health status of the people. The maternal mortality rate, Life expectancy has enhanced especially that of females to over 73 years. Today the infant mortality rate is as low as 16 and the maternal mortality is below 1, which are comparable to that of some of the developed countries.</p> 
 
<h4>Mission & Vision</h4>
<p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;In spite of the achievements made by Kerala with regard to health indicators, the public health scenario in the State was in a state of crisis when this Government took over. Poor infrastructure, inadequate staff, lack of resources and absence of coordinated policy approach to tackle the problems and the flaws in the leadership was the major lacunae facing the sector. The Government started with the objective of revamping the Public Health Institutions and initiated short--term and medium-term measures to address to the problems. In the last three years, Government has been successful in providing a sense of direction to the public health sector and putting the department back on rail. We hope to consolidate the efforts made so far and complete the implementation of the projects in the coming years.</p>
    </div>
  </section>
      </div>
    </section>

  </main><!-- End #main -->
  <?php include('footer.php');?>
